"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import argparse
import datetime
import logging
import re

import pywikibot
from pywikibot import pagegenerators
from pywikibot.exceptions import InconsistentTitleError, InvalidTitleError
from pywikibot.textlib import extract_templates_and_params

WPSV_PAGE = "Wikipedia:Samenvoegen"

REPORT_PAGE = WPSV_PAGE + "/Controlelijst"
LOG_PAGE = REPORT_PAGE + "/log"

ONDERHOUD_CATEGORIE = "Categorie:Wikipedia:Onderhoud"

IGNORE_LIST = [
]

IGNORED_NAMESPACES = [
    pywikibot.site.Namespace.USER,
    pywikibot.site.Namespace.USER_TALK,
    pywikibot.site.Namespace.TALK,
    pywikibot.site.Namespace.PROJECT_TALK,
    pywikibot.site.Namespace.TEMPLATE_TALK,
    pywikibot.site.Namespace.FILE_TALK,
    pywikibot.site.Namespace.MEDIAWIKI_TALK,
    pywikibot.site.Namespace.CATEGORY_TALK,
]

NR_OF_LOG_ITEMS_SHORT_LOG = 7
NR_OF_LOG_ITEMS_FULL_LOG = 30

TEMPLATE_VAN = "Samenvoegenvan"
TEMPLATE_NAAR = "Samenvoegennaar"
TEMPLATE_EN = "Samenvoegen"
TEMPLATE_MULTI = "Multisamenvoegenvan"

TEMPLATE_VAN_ALTS = [
    "Samenvoegenvan",
    "Svvan",
    "Samenvoegen_van",
    "Multisamenvoegenvan",
]
TEMPLATE_NAAR_ALTS = ["Samenvoegennaar", "Svnaar", "Samenvoegen_naar"]
TEMPLATE_EN_ALTS = ["Samenvoegen", "Merge"]

NOMINATION_MSG = "Sjablonen geplakt maar hier nog niet vermeld. Automatisch bericht van"
NOMINATION_SUMMARY = "Robot: ontbrekende nominatie(s) toegevoegd"

ADD_TEMPLATE_SUMMARY = "Robot: ontbrekend samenvoegsjabloon toegevoegd"

REGEX_CANON_REMOVE_SECTION = re.compile(r"#.*")
REGEX_CANON_REMOVE_TRAILING_WS = re.compile(r"\s+$")

TYPE_VAN = 1
TYPE_NAAR = 2
TYPE_EN = 3

DAYS = ["Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag", "Zaterdag", "Zondag"]


def canonize(title):
    title = re.sub(REGEX_CANON_REMOVE_SECTION, "", title)
    title = re.sub(REGEX_CANON_REMOVE_TRAILING_WS, "", title)
    title = title.replace(" ", "_")
    title = title.replace("&#39", "'")

    if title == "":
        return ""

    return title[0].upper() + title[1:]


def pp_type(template_type):
    if template_type == TYPE_VAN:
        return TEMPLATE_VAN
    elif template_type == TYPE_NAAR:
        return TEMPLATE_NAAR
    elif template_type == TYPE_EN:
        return TEMPLATE_EN

    logging.error(f"Invalid template type: {template_type}")
    return ""


def pp_wpsv(page):
    date = None
    if isinstance(page, pywikibot.Page):
        match = re.search(r"\d{6}", page.title())
        if match:
            date = match.group(0)
    elif re.fullmatch(r"\d{6}", page):
        date = page

    if not date:
        logging.warning("Not a wp:sv page: " + str(page))
        return ""

    return "[[" + WPSV_PAGE + "/" + date + "|WP:SV/" + date + "]]"


def set_logging(level):
    loggers = [logging.getLogger(name) for name in logging.root.manager.loggerDict]
    for logger in loggers:
        logger.setLevel(level)

    logging.basicConfig(level=level)


class Samenvoegen:
    def __init__(self):
        self.site = pywikibot.Site()

        self.ignore_list = set()

        self.pages_all = set()
        self.templates = {}  # {page: {(type, target)}}
        self.pairs = set()  # {(page, target, type)}

        self.wpsv_subpages = []
        self.nominated_pages = set()
        self.clear_nominations = set()  # {(page, target, type, page with nomination)}

        self.already_reported = set()

        self._report = []
        self._log = []

        self._queue = set()  # {(page, text_to_insert, nomination_page)}
        self._nomination_que = set()  # {(page, target, type)}

    def get_ignorelist(self):
        for title in IGNORE_LIST:
            self.ignore_list.add(self.page(title))

        txt = self.page(REPORT_PAGE).text

        content = re.search(r" BEGIN-->(.*?)<!--EINDE ", txt, re.X | re.S)

        if content:
            for title in re.findall(r" \[\[ ( [^\[\]]+ ) ]] ", content.group(1), re.X):
                self.ignore_list.add(self.page(canonize(title)))

    def report_header(self):
        return "{{Gebruiker:MrBlueSky/Botpagina}}\n==Pagina's waar iets mee is==\n"

    def report_footer(self):
        s = (
            "==Negeerlijst==\n"
            "<small>Artikelen die hieronder gelinkt zijn worden in het vervolg genegeerd.</small>\n"
            "<!-- Deze regel ongewijzigd laten staan aub. Artikelen hieronder toevoegen --><!--BEGIN-->\n"
        )

        for page in sorted(self.ignore_list):
            s += f"*[[:{page.title()}]]\n"

        s += (
            "* ...\n"
            "<!--EINDE--><!-- Deze regel ongewijzigd laten staan aub. -->\n"
            "==Recente logitems==\n"
            "{{Zie ook|Meer logitems: [["
            + LOG_PAGE
            + "]]}}\n"
            + "<!--logbegin--><!--/logend-->"
            + "[["
            + ONDERHOUD_CATEGORIE
            + "]]\n"
        )

        return s

    def save_report_and_log(self):
        new_report = self.report_header()

        for line in self._report:
            new_report += line + "\n"

        new_report += self.report_footer()

        report_page = self.page(REPORT_PAGE)

        old_report = re.sub(
            r"<!--logbegin-->.*<!--/logend-->",
            "<!--logbegin--><!--/logend-->",
            report_page.text,
            flags=re.X | re.S,
        )

        report_has_changed = False
        if new_report.strip() != old_report.strip():
            self.log("Update [[Wikipedia:Samenvoegen/Controlelijst]]")
            report_has_changed = True

        (full_log, short_log) = self.create_logs()

        new_report = re.sub(
            r"<!--logbegin--><!--/logend-->",
            f"<!--logbegin-->{short_log}<!--/logend-->",
            new_report,
            flags=re.X | re.S,
        )

        if report_has_changed or self._log:
            self.save(report_page, new_report, "Update")

        self.save(self.page(LOG_PAGE), full_log, "Update")

    def load_log(self):
        txt = self.page(LOG_PAGE).text

        return re.findall(
            r" <!--begin([yn])--> ( .*? ) <!--end--> ", txt, re.X | re.S | re.I
        )

    def create_logs(self):
        old_log = self.load_log()

        new_log_item = "=== {{subst:LOCALDAYNAME}} {{subst:LOCALDAY}} {{subst:LOCALMONTHNAME}} ({{subst:LOCALTIME}}) ===\n"

        for line in self._log:
            new_log_item += f"* {line}\n"

        if self._log:
            old_log.insert(0, ("y", new_log_item))
        else:
            new_log_item += "* ''Niks te melden''\n"
            old_log.insert(0, ("n", new_log_item))

        short_log = ""
        full_log = ""

        short_count = 0
        full_count = 0

        for i, log_item in old_log:
            include = i == "y"

            if include:
                full_log += "<!--beginy-->"
            else:
                full_log += "<!--beginn-->"

            full_log += log_item

            if include and short_count < NR_OF_LOG_ITEMS_SHORT_LOG:
                short_log += log_item
                short_count += 1

            full_log += "<!--end-->"
            full_count += 1

            if full_count >= NR_OF_LOG_ITEMS_FULL_LOG:
                break

        return full_log, short_log

    def save(self, page, text, summary):
        if not page.botMayEdit():
            self.log(
                "Ik wil een wijziging aanbrengen op [[:{}]], "
                "maar dat mag niet vanwege [[:Template:Bots|<nowiki>{{{{Bots}}}}</nowiki>]]",
                page.title(),
            )
            return

        page.text = text
        page.save(summary)

    def page(self, title):
        return pywikibot.Page(self.site, title)

    def is_redirect(self, page):
        try:
            return page.isRedirectPage()
        except InconsistentTitleError:
            pass
        except Exception:
            logging.exception()

        return False

    def get_redirect_target(self, page):
        try:
            return page.getRedirectTarget()
        except Exception:
            logging.exception()
            return None

    def exists(self, page):
        try:
            return page.exists()
        except InconsistentTitleError:
            pass
        except Exception:
            logging.exception()

        return False

    def get_backlinks_from_wpsv(self, page):
        result = set()

        for bl in page.backlinks(namespaces=[pywikibot.site.Namespace.PROJECT]):
            match = re.fullmatch(
                r"Samenvoegen/(\d+)", bl.title(with_ns=False), re.X | re.I
            )
            if match:
                if bl in self.wpsv_subpages:
                    result.add(match.group(1))

        return result

    def report(self, msg, *values):
        self._report.append(msg.format(*values))

    def log(self, msg, *values):
        self._log.append(msg.format(*values))

    def load(self):
        self.get_wpsv_subpages()

        pages = []

        for template in TEMPLATE_EN_ALTS + TEMPLATE_VAN_ALTS + TEMPLATE_NAAR_ALTS:
            pages.extend(
                list(
                    self.page("Template:" + canonize(template)).embeddedin(content=True)
                )
            )

        self.pages_all = set(pagegenerators.PreloadingGenerator(pages))

        self.get_nominations()

        for page in self.pages_all:
            templates = self.extract_templates(page)
            self.templates[page] = templates

    def extract_templates(self, text_or_page):
        result = set()
        txt = text_or_page

        if isinstance(text_or_page, pywikibot.Page):
            txt = text_or_page.text

        for template, params in extract_templates_and_params(
            txt, remove_disabled_parts=True
        ):
            template_type = None
            template = canonize(template) # noqa
            if template in TEMPLATE_VAN_ALTS:
                template_type = TYPE_VAN
            elif template in TEMPLATE_NAAR_ALTS:
                template_type = TYPE_NAAR
            elif template in TEMPLATE_EN_ALTS:
                template_type = TYPE_EN

            if template_type:
                for param in params.values():
                    title = canonize(param.strip("[]"))
                    if title:
                        try:
                            result.add((template_type, self.page(title)))
                        except InvalidTitleError:
                            pass

        return result

    def check_sister_templates(self, page):
        if page in self.ignore_list or page.namespace() in IGNORED_NAMESPACES:
            return

        for template_type, target in sorted(self.templates.get(page, set())):
            if target in self.ignore_list or target.namespace() in IGNORED_NAMESPACES:
                continue

            if self.is_redirect(target) and self.get_redirect_target(target) == page:
                self.report(
                    "* '''[[:{}]]''' verwijst naar [[:{}]]",
                    page.title(),
                    target.title(),
                )
                self.report(
                    "::[[:{}]] is een redirect naar [[:{}]]. Mogelijk zijn deze al samengevoegd.",
                    target.title(),
                    page.title(),
                )
                self.already_reported.add(page)
                self.already_reported.add(target)
                continue

            if self.is_redirect(target):
                target = self.get_redirect_target(target) # noqa
                if target in self.ignore_list:
                    continue

            if (
                not self.exists(target)
                and page.namespace() == pywikibot.site.Namespace.MAIN
            ):
                self.report(
                    "* '''[[:{}]]''' verwijst naar [[:{}]] ({})",
                    page.title(),
                    target.title(),
                    pp_type(template_type),
                )
                self.report(":: [[:{}]] bestaat niet.", target.title())
                self.already_reported.add(page)
                continue

            if self.target_has_link_to_this(target, page) or not target.exists():
                self.add_pair(page, target, template_type)
            else:
                other_type = TYPE_EN
                if template_type == TYPE_VAN:
                    other_type = TYPE_NAAR
                elif template_type == TYPE_NAAR:
                    other_type = TYPE_VAN

                self.queue(target, page, other_type)

                self.pairs.add((page, target, template_type))

    def report_nominations_without_template(self):
        missing_templates = set()

        for page, target, template_type, nomination_page in self.clear_nominations:
            self.queue(page, target, template_type, nomination_page)
            if template_type == TYPE_EN:
                self.queue(target, page, TYPE_EN, nomination_page)
            elif template_type == TYPE_VAN:
                self.queue(target, page, TYPE_NAAR, nomination_page)
            elif template_type == TYPE_NAAR:
                self.queue(target, page, TYPE_VAN, nomination_page)

        for page in self.nominated_pages:
            if page in self.ignore_list:
                continue

            if self.is_redirect(page):
                page = self.get_redirect_target(page) # noqa
                if page in self.ignore_list:
                    continue

            if page in self.pages_all:
                continue

            if not self.exists(page):
                continue

            template_found = False

            for target, _, _ in self._queue:
                if target == page:
                    template_found = True

            for p, target, _, _ in self.clear_nominations:
                if page == p or page == target:
                    template_found = True

            if template_found:
                continue

            for template, _ in extract_templates_and_params(
                page.text, remove_disabled_parts=True
            ):
                if (
                    canonize(template)
                    in TEMPLATE_NAAR_ALTS + TEMPLATE_VAN_ALTS + TEMPLATE_EN_ALTS
                ):
                    template_found = True
                    break

            if not template_found:
                missing_templates.add(page)

        for page in sorted(missing_templates):
            bls = sorted(self.get_backlinks_from_wpsv(page))

            bltext = ""
            if bls:
                bltext = "(" + ", ".join(map(pp_wpsv, bls)) + ")"

            self.report("* [[:{}]] {}", page.title(), bltext)

    def process_missing_nominations(self):
        self.remove_nominated_pairs()
        self.resolve_redirects()
        self.remove_nominated_pairs()
        self.uniqify_pairs()

        for page, target, _ in self.pairs:
            if (page.namespace() == pywikibot.site.Namespace.MAIN) != (
                target.namespace() == pywikibot.site.Namespace.MAIN
            ):
                try:
                    self.pages_all.remove(page)
                except KeyError:
                    pass
                try:
                    self.pages_all.remove(target)
                except KeyError:
                    pass

        self.remove_pairs_with_backlinks()

        for page, target, template_type in sorted(self.pairs):
            if page in self.ignore_list or target in self.ignore_list:
                continue

            if self.wpsv_subpages:
                self._nomination_que.add((page, target, template_type))
                self.log(
                    "Samenvoegnominatie voor [[:{}]] en [[:{}]] toegevoegd op [[{}]]",
                    page.title(),
                    target.title(),
                    self.wpsv_subpages[-1].title(),
                )

    def report_misc_problems(self):
        for page in sorted(self.pages_all):
            if page.namespace() != pywikibot.site.Namespace.MAIN:
                continue

            if page in self.ignore_list:
                continue

            if self.has_pair(page):
                continue

            if page in self.nominated_pages:
                continue

            if page in self.already_reported:
                continue

            is_nominated_through_rd = False
            for nominated_page in self.nominated_pages:
                if self.is_redirect(nominated_page):
                    if self.get_redirect_target(nominated_page) == page:
                        is_nominated_through_rd = True
                        break

            if is_nominated_through_rd:
                continue

            bl_text = ""
            backlinks = sorted(self.get_backlinks_from_wpsv(page))
            if backlinks:
                bl_text = "(" + ", ".join(map(pp_wpsv, backlinks)) + ")"

            self.report("* [[:{}]] {}", page.title(), bl_text)

    def add_nominations(self):
        if not self._nomination_que:
            return

        if not self.wpsv_subpages:
            return

        nomination_page = self.wpsv_subpages[-1]
        nomination_text = ""

        for page, target, template_type in self._nomination_que:
            type_str = "samenvoegen met"
            if template_type == TYPE_NAAR:
                type_str = "samenvoegen naar"
            elif template_type == TYPE_VAN:
                type_str = "samenvoegen van"

            nomination_text += "==== [[:{}]] {} [[:{}]] ====\n{} ~~~~\n".format(
                page.title(), type_str, target.title(), NOMINATION_MSG
            )

        new_text = re.sub(
            r" \n* (<noinclude>\s*<!--\s+Hierboven\s+graag[^=]+) ",
            "\n" + nomination_text + r"\n\1",
            nomination_page.text,
            count=1,
            flags=re.X | re.I,
        )

        if new_text == nomination_page.text:
            logging.warning("Could not parse WP:SV:\n")
            return

        self.save(nomination_page, new_text, NOMINATION_SUMMARY)

    def add_templates(self):
        for page, txt_to_insert, nomination_page in self._queue:
            summary = ADD_TEMPLATE_SUMMARY
            if nomination_page:
                summary += f" (genomineerd op {pp_wpsv(nomination_page)})"

            new_text = txt_to_insert + self.page(page.title()).text
            self.save(page, new_text, summary)

    def get_wpsv_subpages(self):
        wpsv_text = self.page(WPSV_PAGE).text

        for title in re.findall(r" {{ ( /  \d\d\d\d\d\d ) }} ", wpsv_text, re.X):
            fq_title = WPSV_PAGE + title
            self.wpsv_subpages.append(self.page(fq_title))

    def get_nominations_lists(self):
        for page in self.wpsv_subpages:
            match = re.search(
                r"===+\s*Nog\s*te\s*doen\s*===+(.+?)===+\s*Afgehandeld",
                page.text,
                re.X | re.I | re.S,
            )

            if match:
                yield match.group(1), page
                continue

            match = re.search(
                r"===+\s*Deel\s1\s*===+(.+?)===+\s*Afgehandeld",
                page.text,
                re.X | re.I | re.S,
            )

            if match:
                yield match.group(1), page
                continue

            logging.warning(f"Could not parse {page.title()}: \n{page.text}\n")

    def get_nominations(self):
        pattern = re.compile(
            r" === \s* ( [^=]+ ) \s* ===",
            re.X | re.I,
        )

        for nominations, nomination_page in self.get_nominations_lists():
            for nomination_text in pattern.findall(nominations):
                match = re.match(
                    r" \[\[ ( [^]]+ ) \]\]  ( [\s\w]+ ) \[\[ ( [^]]+ ) \]\] ",
                    nomination_text,
                    re.X | re.S,
                )

                if match:
                    page = self.parse_link(match.group(1))
                    soort = match.group(2).lower().strip()
                    target = self.parse_link(match.group(3))

                    if self.is_redirect(page):
                        page = self.get_redirect_target(page)
                    if self.is_redirect(target):
                        target = self.get_redirect_target(target)

                    template_type = None
                    if soort in [
                        "naar",
                        "in",
                        "invoegen in",
                        "onderbrengen bij",
                        "samenvoegen in",
                        "samenvoegen naar",
                    ]:
                        template_type = TYPE_NAAR
                    elif soort in ["en", "samenvoegen met"]:
                        template_type = TYPE_EN
                    elif soort in ["van", "samenvoegen van"]:
                        template_type = TYPE_VAN

                    if (
                        page
                        and target
                        and template_type
                        and page not in self.pages_all
                        and target not in self.pages_all
                    ):
                        self.clear_nominations.add(
                            (page, target, template_type, nomination_page)
                        )

                for link_txt in re.findall(
                    r" \[\[ (  [^]]+ ) \]\] ", nomination_text, re.X | re.S
                ):
                    page = self.parse_link(link_txt)
                    if page and page.site == self.site:
                        self.nominated_pages.add(page)

        clear_nominations_filtered = set()
        for page, target, template_type, nomination_page in self.clear_nominations:
            found_duplicate = False
            for page2, target2, _, nomination_page2 in self.clear_nominations:
                if page == page2 and target == target2:
                    if nomination_page2.title() < nomination_page.title():
                        clear_nominations_filtered.add(
                            (page, target, template_type, nomination_page)
                        )
                        found_duplicate = True
                    elif nomination_page != nomination_page2:
                        found_duplicate = True
            if not found_duplicate:
                clear_nominations_filtered.add(
                    (page, target, template_type, nomination_page)
                )

        self.clear_nominations = clear_nominations_filtered

    def parse_link(self, txt):
        try:
            txt = re.sub(r"#.*", "", txt)
            return pywikibot.Page(pywikibot.Link(txt, self.site))
        except Exception as e: #noqa
            logging.debug(str(e))
            return None

    def target_has_link_to_this(self, page, target_page):
        for _, target in self.templates.get(page, set()):
            if target == target_page:
                return True
            elif (
                self.is_redirect(target)
                and self.get_redirect_target(target) == target_page
            ):
                return True

        return False

    def add_pair(self, page, target, template_type):
        if not page.exists() or not target.exists():
            return

        if template_type == TYPE_VAN:
            self.pairs.add((target, page, template_type))
        else:
            self.pairs.add((page, target, template_type))

    def has_pair(self, page):
        return any(p == page or target == page for p, target, _ in self.pairs)

    def remove_nominated_pairs(self):
        for page in self.nominated_pages:
            self.remove_pairs(page)

    def remove_pairs(self, page):
        self.pairs = set(filter(lambda p: p[0] != page and p[1] != page, self.pairs))

    def resolve_redirects(self):
        for pair in self.pairs.copy():
            if self.is_redirect(pair[1]):
                rd_target = self.get_redirect_target(pair[1])
                if rd_target:
                    try:
                        self.pairs.remove(pair)
                    except KeyError:
                        pass
                    self.pairs.add((pair[0], rd_target, pair[2]))

    def uniqify_pairs(self):
        result = set()

        for first_pair in self.pairs:
            is_present = False
            for second_pair in result:
                if first_pair[0] == second_pair[1] and first_pair[1] == second_pair[0]:
                    is_present = True
                if first_pair[0] == second_pair[0] and first_pair[1] == second_pair[1]:
                    is_present = True

            if not is_present:
                result.add(first_pair)

        self.pairs = result

    def remove_pairs_with_backlinks(self):
        result = set()

        for pair in self.pairs:
            source = pair[0]
            target = pair[1]

            if not self.get_backlinks_from_wpsv(
                source
            ) and not self.get_backlinks_from_wpsv(target):
                result.add(pair)

        self.pairs = result

    def queue(self, page, target, template_type, nomination_page=None):
        if page == target:
            return

        if page in self.ignore_list or target in self.ignore_list:
            return

        if not nomination_page or nomination_page.title().endswith(
            datetime.date.today().strftime("%Y%m")
        ):
            if page.botMayEdit():
                self._queue.add(
                    (
                        page,
                        "{{" + pp_type(template_type) + "|" + target.title() + "}}\n",
                        nomination_page,
                    )
                )
                log_msg = f"Sjabloon ''{pp_type(template_type)}'' toegevoegd op [[:{page.title()}]]"
                if nomination_page:
                    log_msg += f" (genomineerd op {pp_wpsv(nomination_page)})"
                self.log(log_msg)
            else:
                self.report(
                    "* Geen sjabloon toegevoegd op [[:{}]], "
                    "vanwege [[:Sjabloon:Bots|<nowiki>{{{{Bots}}}}</nowiki>]].",
                    page.title(),
                )

        elif page.exists():
            self.report(
                "* [[:{}]] is genomineerd op [[{}]] maar bevat geen sjabloon. Mogelijk al afgehandeld maar nog niet "
                "verplaatst.".format(page.title(), nomination_page.title())
            )
        else:
            self.report(
                f"* [[:{page.title()}]] is genomineerd op [[{nomination_page.title()}]] maar bestaat niet."
            )

    def run(self):
        self.get_ignorelist()
        self.load()

        for page in sorted(self.pages_all):
            self.check_sister_templates(page)
            for template_type, target in self.templates.get(page, set()):
                if template_type == TYPE_EN or template_type == TYPE_NAAR:
                    self.add_pair(page, target, template_type)

        self.report("==Pagina's op [[WP:SV]] zonder samenvoegsjabloon==")
        self.report_nominations_without_template()
        self.add_templates()

        self.process_missing_nominations()
        self.add_nominations()

        self.report("==Onduidelijk==")
        self.report_misc_problems()

        self.save_report_and_log()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-debug", action="store_true")

    args = parser.parse_args()

    if args.debug:
        set_logging(logging.DEBUG)
    else:
        set_logging(logging.WARN)

    Samenvoegen().run()
