This bot checks for missing 'samenvoegen' nominations and templates and adds them where possible.
See [Wikipedia:Samenvoegen/Controlelijst](https://nl.wikipedia.org/wiki/Wikipedia:Samenvoegen/Controlelijst).

* Runs: every day
* License: Unlicense
* Python packages needed: pywikibot
