This bot updates the [Wikipedia:Onderhoud/haakjes/*](https://nl.wikipedia.org/wiki/Speciaal:Voorvoegselindex?prefix=Wikipedia%3AOnderhoud%2Fhaakjes%2F&namespace=0) lists, 
with information about titles that contain parentheses.

* Runs: every 5 days
* License: Unlicense
* Python packages needed: pymysql, pywikibot, toolforge
