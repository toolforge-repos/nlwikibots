# This is free and unencumbered software released into the public domain.
# See the file LICENSE for more details, or visit <http://unlicense.org>.

import re
from collections import OrderedDict, defaultdict

import pymysql
import pywikibot
import toolforge
from pywikibot import Link, Page, Site, pagegenerators
from pywikibot.exceptions import InvalidTitleError, SiteDefinitionError

IGNORE_LIST = "Onderhoud/haakjes/negeerlijst"
IGNORE_LIST_MFR = "Onderhoud/haakjes/Mogelijk_Foute_Redirects/negeerlijst"
BASE_PAGE = "Wikipedia:Onderhoud/haakjes"
BASE_PAGE_ZONDER_HAAKJES_BESTAAT_NIET = f"{BASE_PAGE}/GeenArtikelZonderHaakjes"
BASE_PAGE_ZONDER_HAAKJES_GEEN_DP = f"{BASE_PAGE}/ZonderHaakjesGeenDP"
FOUTE_REDIRECTS_PAGE = f"{BASE_PAGE}/Mogelijk_Foute_Redirects"

UPDATE_SUMMARY = "Update"

NR_OF_TITLES_PER_PAGE = 2500

DOORVERWIJSPAGINA_SUFFIX = " (doorverwijspagina)"

ONDER_HOUD_CATEGORIE = "Categorie:Wikipedia:Onderhoud"
BOT_HEADER_TEMPLATE = "Gebruiker:MrBlueSky/Botpagina"

REGEX_WIKI_LINK = re.compile(r" \[\[ ( [^|\]]+ ) [|\]] ", re.X)

REGEX_ZIE_ARTIKEL = re.compile(
    r" {{\s* zie \s? artikel \s* \| ( [^}]+ ) \}\}", re.X | re.I
)
REGEX_ZIE_HOOFDARTIKEL = re.compile(
    r" {{ \s* zie \s? hoofdartikel \s* \|  ( [^}]+ ) \}\} ", re.X | re.I
)
REGEX_ZIE_OOK = re.compile(r" {{ \s* zie \s? ook \s* \|  ( [^}]+ ) \}\} ", re.X | re.I)

REGEX_HAAKJES_TITLE = re.compile(r"(  [^(]+  )  \(  .*  \) $", re.X)

REGEX_GET_SUFFIX_PRE = re.compile(r"^[^(]+\(")
REGEX_GET_SUFFIX_POST = re.compile(r"\).*$")

REGEX_CANON_REMOVE_SECTION = re.compile(r"#.*")
REGEX_CANON_REMOVE_TRAILING_WS = re.compile(r"\s+$")

Q_REDIRECTS = """
    SELECT
        `page_title`
    FROM
        `page`
    WHERE
        `page_is_redirect`=1 AND `page_namespace`=0
"""

Q_NON_REDIRECTS = """
    SELECT
        `page_title`,`page_id`
    FROM
        `page`
    WHERE
        `page_is_redirect`=0 AND `page_namespace`=0
"""

Q_DPS = """
   SELECT
        `page`.`page_title`
    FROM
        `page`
    JOIN
        `templatelinks`
    ON
        `page`.`page_id`=`templatelinks`.`tl_from`
    JOIN
        `linktarget`
    ON
        `templatelinks`.`tl_target_id` = `linktarget`.`lt_id`
    WHERE
        `linktarget`.`lt_title`='Dp' AND `page`.`page_namespace`=0 AND `linktarget`.`lt_namespace`=10
"""

Q_IGNORE_LIST = """
SELECT
        `linktarget`.`lt_title`
    FROM
        `page`
	JOIN
		`pagelinks`
	ON
		`page`.`page_id` = `pagelinks`.`pl_from`
    JOIN
        `linktarget`
    ON
		`linktarget`.`lt_id` = `pagelinks`.`pl_target_id`
    WHERE
        `page`.`page_title` = '{}' AND `page`.`page_namespace`=4
"""

HEADER_COMMON = f"""
{{{{{BOT_HEADER_TEMPLATE}}}}}
*[[Wikipedia:{IGNORE_LIST}|Negeerlijst]]
[[{ONDER_HOUD_CATEGORIE}]]
"""

HEADER_ZONDER_HAAKJES_BESTAAT_NIET = (
    HEADER_COMMON
    + """
{| class='wikitable sortable'
! Zonder haakjes
! Met haakjes
! Suffix
! Afhandeling
"""
)

HEADER_ZONDER_HAAKJES_GEEN_DP = (
    HEADER_COMMON
    + """
* Groen: de pagina zonder haakjes bevat {{tl|zie ook}}s of {{tl|zie artikel}}s naar alle titels met haakjes
* Geel: de pagina bevat links (gewone, zieartikels of zieooks) naar alle titels met haakjes
* Wit: niet alle titels met haakjes zijn gelinkt vanaf de pagina zonder haakjes
{| class='wikitable sortable'
! Zonder haakjes
! Met haakjes
! Suffix
"""
)

HEADER_FOUTER_REDIRECTS = f"""
{{{{{BOT_HEADER_TEMPLATE}}}}}
*[[Wikipedia:{IGNORE_LIST_MFR}|Negeerlijst]]
[[{ONDER_HOUD_CATEGORIE}]]
{{| class='wikitable sortable'
! Redirect
! Huidige doel
! Mogelijke doelen
! Afhandeling
"""

KEY_IGNORE_LIST = "ignore"
KEY_IGNORE_LIST_MFR = "ignore_mfr"
KEY_REDIRECT_TITLES = "redirect"
KEY_NON_REDIRECT_TITLES = "non_redirect"
KEY_DPS = "dps"
KEY_NAMESPACES = "ns"
KEY_PAGE_CONTENTS = "contents"
KEY_RD_CACHE = "rd_cache"


def canonize(title):
    title = re.sub(REGEX_CANON_REMOVE_SECTION, "", title)
    title = re.sub(REGEX_CANON_REMOVE_TRAILING_WS, "", title)
    title = title.replace(" ", "_")
    title = title.replace("&#39", "'")

    if title == "":
        return ""

    return title[0].upper() + title[1:]


def pp(title):
    return title.replace("_", " ")


def get_suffix(title):
    title = re.sub(REGEX_GET_SUFFIX_PRE, "", title)
    return re.sub(REGEX_GET_SUFFIX_POST, "", title)


def extract_namespace(title):
    if title.casefold().startswith("iarchive:"):
        return 99
    try:
        return Link(title).namespace.id
    except (InvalidTitleError, SiteDefinitionError, ValueError):
        return 0


class Haakjes:
    def __init__(self):

        self.redirect_titles = set()
        self.non_redirect_titles = {}
        self.dps = []
        self.ignore_list = []
        self.ignore_list_mfr = []

        self.valid_namespaces = {}

        self.redirects_to_load = set()

        self.page_contents = {}
        self.redirect_targets = {}

        self.bestaat_niet_result = defaultdict(list)
        self.geen_dp_result = defaultdict(list)
        self.foute_redirects = defaultdict(list)

        self.site = Site()

    def connect(self):
        return toolforge.connect("nlwiki_p", cursorclass=pymysql.cursors.DictCursor)

    def load(self):
        self.valid_namespaces = self.site.namespaces
        self.ignore_list = self.load_ignore_list(IGNORE_LIST)
        self.ignore_list_mfr = self.load_ignore_list(IGNORE_LIST_MFR)
        self.redirect_titles = set(self.load_titles(Q_REDIRECTS))
        self.non_redirect_titles = self.load_non_redirect_titles()
        self.dps = self.load_titles(Q_DPS)

    def load_ignore_list(self, title):
        return self.load_titles(Q_IGNORE_LIST.format(title), key="lt_title")

    def preload_page_contents(self, titles):
        generator = pagegenerators.PreloadingGenerator(
            pagegenerators.PagesFromTitlesGenerator(titles, site=self.site)
        )

        for page in generator:
            title = page.title(underscore=True, with_ns=True)
            if title not in self.page_contents:
                self.page_contents[title] = page.text

    def load_non_redirect_titles(self):
        with self.connect().cursor() as cursor:
            cursor.execute(str(Q_NON_REDIRECTS))
            return {
                title["page_title"].decode("utf-8"): title["page_id"]
                for title in cursor.fetchall()
            }

    def load_titles(self, query, key="page_title"):
        with self.connect().cursor() as cursor:
            cursor.execute(str(query))
            return [title[key].decode("utf-8") for title in cursor.fetchall()]

    def calc_zonder_haakjes_bestaat_niet(self):

        for title in self.non_redirect_titles:

            if title in self.ignore_list:
                continue

            match = REGEX_HAAKJES_TITLE.match(title)
            if match:
                base_title = match.group(1).strip().strip("_")
                if (
                    base_title not in self.non_redirect_titles
                    and base_title not in self.redirect_titles
                ):
                    self.bestaat_niet_result[base_title].append(title)

        self.bestaat_niet_result = OrderedDict(sorted(self.bestaat_niet_result.items()))

    def calc_zonder_haakjes_geen_dp(self):

        for title in self.non_redirect_titles:

            if "doorverwijspagina" in str(title).lower():
                continue

            match = REGEX_HAAKJES_TITLE.match(title)
            if match:
                base_title = match.group(1).strip().strip("_")
                if base_title not in self.redirect_titles:
                    if base_title in self.non_redirect_titles:
                        if (
                            base_title + "_(doorverwijspagina)"
                            not in self.non_redirect_titles
                        ):
                            if base_title not in self.dps:
                                self.geen_dp_result[base_title].append(title)

        self.geen_dp_result = OrderedDict(sorted(self.geen_dp_result.items()))

    def calc_foute_redirects(self):

        for title in self.non_redirect_titles:

            match = REGEX_HAAKJES_TITLE.match(title)

            if match:
                base_title = match.group(1).strip().strip("_")
                if base_title in self.redirect_titles:
                    self.foute_redirects[base_title].append(title)

        self.foute_redirects = {
            base_title: titles
            for base_title, titles in self.foute_redirects.items()
            if len(titles) > 1
        }

        redirect_targets = self.resolve_redirects(self.foute_redirects.keys())

        self.foute_redirects = {
            base_title: titles
            for base_title, titles in self.foute_redirects.items()
            if redirect_targets.get(base_title, None) not in self.dps
        }

    def calc_color(self, base, titles):
        zieooks = set(self.parse_zieooks(self.page_contents[base]))
        plain_links = set(self.parse_links(self.page_contents[base]))
        redirects_to_get = set(
            filter(lambda x: x in self.redirect_titles, zieooks | plain_links)
        )
        redirects = self.resolve_redirects(redirects_to_get)

        color = 2

        for title in titles:
            flag = 0
            for zieook in zieooks:
                zieook = redirects.get(zieook, zieook) # noqa
                if zieook == title:
                    flag = 1
                    break

            if flag == 0:
                color = 1
                break

        for title in titles:
            flag = 0
            for link in plain_links:
                link = redirects.get(link, link) # noqa
                if link == title:
                    flag = 1
                    break

            if flag == 0:
                color = 0
                break

        return color

    def resolve_redirects(self, titles):

        result = {title: self.redirect_targets.get(title, None) for title in titles}
        to_get = list(filter(lambda x: not result[x], result.keys()))
        if to_get:
            result.update(self.get_redirect_targets(to_get))
            self.redirect_targets.update(result)
        return result

    def get_redirect_targets(self, titles):

        redirects = {}

        for i in range(0, len(titles), 50):
            # noinspection PyUnresolvedReferences
            gen = pywikibot.data.api.Request(
                site=self.site,
                parameters={
                    "action": "query",
                    "redirects": True,
                    "titles": map(lambda x: x.replace("_", " "), titles[i : i + 50]), # noqa
                },
            )

            data = gen.submit()

            r = {
                x["from"].replace(" ", "_"): x["to"].replace(" ", "_")
                for x in data["query"]["redirects"]
            }

            redirects.update(r)

        result = {}

        for title in titles:
            count = 0
            target = title
            while count < 4:
                try:
                    target = redirects[target]
                except KeyError:
                    break
            result[title] = target

        return result

    def parse_links(self, txt):
        for match in REGEX_WIKI_LINK.finditer(txt):
            title = canonize(match.group(1))
            ns = extract_namespace(title)

            if ns == 0:
                yield title

    def parse_zieooks(self, txt):

        result = set()

        for template_content in REGEX_ZIE_ARTIKEL.findall(txt):
            for target in REGEX_WIKI_LINK.findall(template_content):
                result.add(canonize(target))

        for target in REGEX_ZIE_HOOFDARTIKEL.findall(txt):
            result.add(canonize(target))

        for template_content in REGEX_ZIE_OOK.findall(txt):
            for target in REGEX_WIKI_LINK.findall(template_content):
                result.add(canonize(target))

        return filter(lambda x: extract_namespace(x) == 0, result)

    def report_foute_redirects(self):

        report = HEADER_FOUTER_REDIRECTS

        for base_title, titles in sorted(self.foute_redirects.items()):

            if base_title in self.ignore_list_mfr:
                continue

            target = self.redirect_targets.get(base_title, "")
            report += f"|-\n| [[{pp(base_title)}]] || [[{pp(target)}]] || "
            report += ", ".join(map(lambda x: f"[[{pp(x)}]]", sorted(titles))) # noqa
            report += f" ({len(titles)}) || \n"

        report += "|}"

        page = Page(self.site, FOUTE_REDIRECTS_PAGE)

        self.do_report(page, report, UPDATE_SUMMARY)

    def report_zonder_haakjes_geen_dp(self):
        pagecount = 0
        total = len(self.geen_dp_result)
        startindex = 0

        while startindex < total:

            report = HEADER_ZONDER_HAAKJES_GEEN_DP
            pagecount += 1

            index = startindex

            while index < startindex + NR_OF_TITLES_PER_PAGE and index < total:

                # noinspection PyArgumentList
                base, titles = self.geen_dp_result.popitem(last=False)

                titles.sort()

                suffix = get_suffix(titles[0])

                color = self.calc_color(base, titles)

                del self.page_contents[base]

                if color == 1:
                    report += "|- style='background-color:yellow'\n"
                elif color == 2:
                    report += "|- style='background-color:lightgreen'\n"
                else:
                    report += "|-\n"

                if len(titles) == 1:
                    report += "| [[{}]] || [[{}]] || {} \n".format(
                        pp(base), pp(titles[0]), pp(suffix)
                    )
                else:
                    report += f"| [[{pp(base)}]] || "
                    report += ", ".join(map(lambda x: f"[[{pp(x)}]]", titles)) # noqa
                    report += f" ({len(titles)}) || {pp(suffix)} \n"

                index += 1

            report += "|}"

            page = Page(self.site, BASE_PAGE_ZONDER_HAAKJES_GEEN_DP + str(pagecount))

            self.do_report(page, report, UPDATE_SUMMARY)

            startindex += NR_OF_TITLES_PER_PAGE

    def report_zonder_haakjes_bestaat_niet(self):
        pagecount = 0
        total = len(self.bestaat_niet_result)
        startindex = 0

        while startindex < total:

            report = HEADER_ZONDER_HAAKJES_BESTAAT_NIET
            pagecount += 1

            index = startindex

            while index < startindex + NR_OF_TITLES_PER_PAGE and index < total:
                # noinspection PyArgumentList
                base, titles = self.bestaat_niet_result.popitem(last=False)

                titles.sort()
                suffix = get_suffix(titles[0])

                if len(titles) == 1:
                    report += "|-\n| [[{}]] || [[{}]] || {} || \n".format(
                        pp(base), pp(titles[0]), pp(suffix)
                    )
                else:
                    report += f"|-\n| [[{pp(base)}]] || "
                    report += ", ".join(map(lambda x: f"[[{pp(x)}]]", titles)) # noqa
                    report += f" ({len(titles)}) || {pp(suffix)} || \n"

                index += 1

            report += "|}"

            page = Page(
                self.site, BASE_PAGE_ZONDER_HAAKJES_BESTAAT_NIET + str(pagecount)
            )

            self.do_report(page, report, UPDATE_SUMMARY)

            startindex += NR_OF_TITLES_PER_PAGE

    def do_report(self, page, txt, summary):
        page.text = txt
        page.save(summary)

    def run(self):
        self.load()

        self.calc_zonder_haakjes_bestaat_niet()
        self.calc_zonder_haakjes_geen_dp()
        self.calc_foute_redirects()

        self.preload_page_contents(self.geen_dp_result.keys())

        self.report_foute_redirects()
        del self.foute_redirects
        self.report_zonder_haakjes_bestaat_niet()
        del self.bestaat_niet_result

        self.report_zonder_haakjes_geen_dp()


if __name__ == "__main__":
    haakjes = Haakjes()
    haakjes.run()
