This bot updates [Wikipedia:Links naar doorverwijspagina's/Amsterdamconstructie](https://nl.wikipedia.org/wiki/Wikipedia:Links_naar_doorverwijspagina%27s/Amsterdamconstructie).

* Runs: 3 times per month
* License: Unlicense
* Python packages needed: pymysql, pywikibot, toolforge
