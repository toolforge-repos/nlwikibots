# This is free and unencumbered software released into the public domain.
# See the file LICENSE for more details, or visit <http://unlicense.org>.

import pymysql
import toolforge
from pywikibot import Page, Site

REPORT_PAGE = "Wikipedia:Links naar doorverwijspagina's/Amsterdamconstructie"
SUMMARY = "Update"

Q_DOORVERWIJSPAGINAS = """
    SELECT
        `page`.`page_title` AS title,
        COUNT(`pagelinks`.`pl_target_id`) AS count
    FROM
        `page`
    JOIN
        `linktarget`
    ON
        `linktarget`.`lt_title` = REPLACE(`page`.`page_title`, '_(doorverwijspagina)', '') AND
        `linktarget`.`lt_namespace` = 0
	JOIN
		`pagelinks`
	ON
		`linktarget`.`lt_id` = `pagelinks`.`pl_target_id`
    WHERE
        `page`.`page_title` LIKE '%_(doorverwijspagina)' AND
        `page`.`page_namespace` = 0
    GROUP BY `page`.`page_title`
"""

HEADER = """
{{Gebruiker:MrBlueSky/Botpagina}}

Zie de overlegpagina voor discussie over onderstaande lijst.

{| class="wikitable sortable"
! #
! Dp
! Hoofdbetekenis
! Links naar hoofdbetekenis
"""


class Amsterdam:
    def connect(self):
        return toolforge.connect("nlwiki_p", cursorclass=pymysql.cursors.DictCursor)

    def run(self):
        output = HEADER
        index = 0
        with self.connect().cursor() as cursor:
            cursor.execute(Q_DOORVERWIJSPAGINAS)
            for row in cursor:
                index += 1
                title = row["title"].decode("utf-8")
                output += "|-\n"
                output += f"| {index} \n"
                output += f"| [[{title}]]\n"
                output += f"| [[{title[:-20]}]]\n"
                output += "| {}\n".format(row["count"])
        output += "|}\n"
        page = Page(Site(), REPORT_PAGE)
        page.text = output
        page.save(SUMMARY)


if __name__ == "__main__":
    amsterdam = Amsterdam()
    amsterdam.run()
