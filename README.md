nlwikibots is een project op Tools Labs. Het account is bedoeld voor bots, scripts of webtools waar Wikipediagebruikers veel waarde aan hechten. Denk daarbij aan onderhoudstaken of het publiceren van databasegegevens.

Het doel is dat zulke bots niet meer per se door één persoon onderhouden moeten worden. Dat betekent niet dat alles door meerdere gebruikers onderhouden moet worden, maar vooral dat de continuïteit beter gewaarborgd wordt.

Voor meer informatie: zie https://nl.wikipedia.org/wiki/Wikipedia:Nlwikibots

De gebruikte licenties variëren, maar zijn in het algemeen MIT of CC-BY-SA.

De code in dit repository komt deels uit https://phabricator.wikimedia.org/diffusion/TSVN/browse/erwin85/trunk/ en https://github.com/valhallasw/tvpupdater

----

### Setup van projectmappen

```
.
├── .pywikibot                     # alle geheime info, map is chmod 700, bestanden hierin in principe 600
│   ├── botpasswords               # botpasswords voor alle bot user accounts in bots/nlwikibots
│   ├── passwords                  # gewone passwords
│   └── user-config.py             # configuratie site, user accounts, etc
│
├── bots
│   ├── nlwikibots                 # deze repository
│   │   ├── README.md
│   │   ├── bots                   # sourcecode van alle bots
│   │   └── k8s-jobs.yaml          # definitie van alle automatisch draaiende jobs
│   ├── potd-rotate                # alle code en configuratie van potd-rotate
│   └── tbpmelder                  # alle code en configuratie van tbpmelder
│
└── venv-tf-python39               # gedeelde virtual environment
```

De homedir is op 31 mei 2024 opgeschoond - oude bestanden zijn naar _cleanup_20240531 verplaatst.

### Toevoegen van packages aan gedeelde virtualenv

- Open een shell in de juiste container met `webservice --backend=kubernetes python3.9 shell`
- `source venv-tf-python39/bin/activate`
- `pip install [...]`

### Updaten van cronjobs

- Edit `k8s-jobs.yaml` in deze repo
- Laad de nieuwe file met `toolforge-jobs load k8s-jobs.yaml`


